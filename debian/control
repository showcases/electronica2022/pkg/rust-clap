Source: rust-clap
Section: rust
Priority: optional
Build-Depends: debhelper (>= 12),
 dh-cargo (>= 25),
 cargo:native <!nocheck>,
 rustc:native <!nocheck>,
 libstd-rust-dev <!nocheck>,
 librust-atty-0.2+default-dev <!nocheck>,
 librust-bitflags-1+default-dev (>= 1.2-~~) <!nocheck>,
 librust-clap-lex-0.2+default-dev (>= 0.2.2-~~) <!nocheck>,
 librust-indexmap-1+default-dev <!nocheck>,
 librust-indexmap-1+std-dev <!nocheck>,
 librust-once-cell-1+default-dev (>= 1.12.0-~~) <!nocheck>,
 librust-strsim-0.10+default-dev <!nocheck>,
 librust-termcolor-1+default-dev (>= 1.1.1-~~) <!nocheck>,
 librust-textwrap-0.15-dev <!nocheck>
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Sylvestre Ledru <sylvestre@debian.org>
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/clap]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/clap
Rules-Requires-Root: no

Package: librust-clap-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-bitflags-1+default-dev (>= 1.2-~~),
 librust-clap-lex-0.2+default-dev (>= 0.2.2-~~),
 librust-indexmap-1+default-dev,
 librust-once-cell-1+default-dev (>= 1.12.0-~~),
 librust-textwrap-0.15-dev
Recommends:
 librust-clap+default-dev (= ${binary:Version})
Suggests:
 librust-clap+atty-dev (= ${binary:Version}),
 librust-clap+backtrace-dev (= ${binary:Version}),
 librust-clap+clap-derive-dev (= ${binary:Version}),
 librust-clap+color-dev (= ${binary:Version}),
 librust-clap+debug-dev (= ${binary:Version}),
 librust-clap+regex-dev (= ${binary:Version}),
 librust-clap+std-dev (= ${binary:Version}),
 librust-clap+strsim-dev (= ${binary:Version}),
 librust-clap+termcolor-dev (= ${binary:Version}),
 librust-clap+terminal-size-dev (= ${binary:Version}),
 librust-clap+unicase-dev (= ${binary:Version}),
 librust-clap+unicode-dev (= ${binary:Version}),
 librust-clap+wrap-help-dev (= ${binary:Version}),
 librust-clap+yaml-rust-dev (= ${binary:Version})
Provides:
 librust-clap+cargo-dev (= ${binary:Version}),
 librust-clap+deprecated-dev (= ${binary:Version}),
 librust-clap+env-dev (= ${binary:Version}),
 librust-clap+unstable-grouped-dev (= ${binary:Version}),
 librust-clap+unstable-replace-dev (= ${binary:Version}),
 librust-clap-3-dev (= ${binary:Version}),
 librust-clap-3+cargo-dev (= ${binary:Version}),
 librust-clap-3+deprecated-dev (= ${binary:Version}),
 librust-clap-3+env-dev (= ${binary:Version}),
 librust-clap-3+unstable-grouped-dev (= ${binary:Version}),
 librust-clap-3+unstable-replace-dev (= ${binary:Version}),
 librust-clap-3.2-dev (= ${binary:Version}),
 librust-clap-3.2+cargo-dev (= ${binary:Version}),
 librust-clap-3.2+deprecated-dev (= ${binary:Version}),
 librust-clap-3.2+env-dev (= ${binary:Version}),
 librust-clap-3.2+unstable-grouped-dev (= ${binary:Version}),
 librust-clap-3.2+unstable-replace-dev (= ${binary:Version}),
 librust-clap-3.2.22-dev (= ${binary:Version}),
 librust-clap-3.2.22+cargo-dev (= ${binary:Version}),
 librust-clap-3.2.22+deprecated-dev (= ${binary:Version}),
 librust-clap-3.2.22+env-dev (= ${binary:Version}),
 librust-clap-3.2.22+unstable-grouped-dev (= ${binary:Version}),
 librust-clap-3.2.22+unstable-replace-dev (= ${binary:Version})
Description: Rust Command Line Argument Parser - Rust source code
 This package contains the source for the Rust clap crate, packaged by debcargo
 for use with cargo and dh-cargo.

Package: librust-clap+atty-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-clap-dev (= ${binary:Version}),
 librust-atty-0.2+default-dev
Provides:
 librust-clap-3+atty-dev (= ${binary:Version}),
 librust-clap-3.2+atty-dev (= ${binary:Version}),
 librust-clap-3.2.22+atty-dev (= ${binary:Version})
Description: Rust Command Line Argument Parser - feature "atty"
 This metapackage enables feature "atty" for the Rust clap crate, by pulling in
 any additional dependencies needed by that feature.

Package: librust-clap+backtrace-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-clap-dev (= ${binary:Version}),
 librust-backtrace-0.3+default-dev
Provides:
 librust-clap-3+backtrace-dev (= ${binary:Version}),
 librust-clap-3.2+backtrace-dev (= ${binary:Version}),
 librust-clap-3.2.22+backtrace-dev (= ${binary:Version})
Description: Rust Command Line Argument Parser - feature "backtrace"
 This metapackage enables feature "backtrace" for the Rust clap crate, by
 pulling in any additional dependencies needed by that feature.

Package: librust-clap+clap-derive-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-clap-dev (= ${binary:Version}),
 librust-clap-derive-3.2.18+default-dev
Provides:
 librust-clap+derive-dev (= ${binary:Version}),
 librust-clap-3+clap-derive-dev (= ${binary:Version}),
 librust-clap-3+derive-dev (= ${binary:Version}),
 librust-clap-3.2+clap-derive-dev (= ${binary:Version}),
 librust-clap-3.2+derive-dev (= ${binary:Version}),
 librust-clap-3.2.22+clap-derive-dev (= ${binary:Version}),
 librust-clap-3.2.22+derive-dev (= ${binary:Version})
Description: Rust Command Line Argument Parser - feature "clap_derive" and 1 more
 This metapackage enables feature "clap_derive" for the Rust clap crate, by
 pulling in any additional dependencies needed by that feature.
 .
 Additionally, this package also provides the "derive" feature.

Package: librust-clap+color-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-clap-dev (= ${binary:Version}),
 librust-clap+atty-dev (= ${binary:Version}),
 librust-clap+termcolor-dev (= ${binary:Version})
Provides:
 librust-clap-3+color-dev (= ${binary:Version}),
 librust-clap-3.2+color-dev (= ${binary:Version}),
 librust-clap-3.2.22+color-dev (= ${binary:Version})
Description: Rust Command Line Argument Parser - feature "color"
 This metapackage enables feature "color" for the Rust clap crate, by pulling in
 any additional dependencies needed by that feature.

Package: librust-clap+debug-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-clap-dev (= ${binary:Version}),
 librust-clap+backtrace-dev (= ${binary:Version}),
 librust-clap-derive-3.2.18+debug-dev
Provides:
 librust-clap-3+debug-dev (= ${binary:Version}),
 librust-clap-3.2+debug-dev (= ${binary:Version}),
 librust-clap-3.2.22+debug-dev (= ${binary:Version})
Description: Rust Command Line Argument Parser - feature "debug"
 This metapackage enables feature "debug" for the Rust clap crate, by pulling in
 any additional dependencies needed by that feature.

Package: librust-clap+default-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-clap-dev (= ${binary:Version}),
 librust-clap+std-dev (= ${binary:Version}),
 librust-clap+color-dev (= ${binary:Version}),
 librust-clap+suggestions-dev (= ${binary:Version})
Provides:
 librust-clap-3+default-dev (= ${binary:Version}),
 librust-clap-3.2+default-dev (= ${binary:Version}),
 librust-clap-3.2.22+default-dev (= ${binary:Version})
Description: Rust Command Line Argument Parser - feature "default"
 This metapackage enables feature "default" for the Rust clap crate, by pulling
 in any additional dependencies needed by that feature.

Package: librust-clap+regex-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-clap-dev (= ${binary:Version}),
 librust-regex-1+default-dev
Provides:
 librust-clap-3+regex-dev (= ${binary:Version}),
 librust-clap-3.2+regex-dev (= ${binary:Version}),
 librust-clap-3.2.22+regex-dev (= ${binary:Version})
Description: Rust Command Line Argument Parser - feature "regex"
 This metapackage enables feature "regex" for the Rust clap crate, by pulling in
 any additional dependencies needed by that feature.

Package: librust-clap+std-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-clap-dev (= ${binary:Version}),
 librust-indexmap-1+std-dev
Provides:
 librust-clap-3+std-dev (= ${binary:Version}),
 librust-clap-3.2+std-dev (= ${binary:Version}),
 librust-clap-3.2.22+std-dev (= ${binary:Version})
Description: Rust Command Line Argument Parser - feature "std"
 This metapackage enables feature "std" for the Rust clap crate, by pulling in
 any additional dependencies needed by that feature.

Package: librust-clap+strsim-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-clap-dev (= ${binary:Version}),
 librust-strsim-0.10+default-dev
Provides:
 librust-clap+suggestions-dev (= ${binary:Version}),
 librust-clap-3+strsim-dev (= ${binary:Version}),
 librust-clap-3+suggestions-dev (= ${binary:Version}),
 librust-clap-3.2+strsim-dev (= ${binary:Version}),
 librust-clap-3.2+suggestions-dev (= ${binary:Version}),
 librust-clap-3.2.22+strsim-dev (= ${binary:Version}),
 librust-clap-3.2.22+suggestions-dev (= ${binary:Version})
Description: Rust Command Line Argument Parser - feature "strsim" and 1 more
 This metapackage enables feature "strsim" for the Rust clap crate, by pulling
 in any additional dependencies needed by that feature.
 .
 Additionally, this package also provides the "suggestions" feature.

Package: librust-clap+termcolor-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-clap-dev (= ${binary:Version}),
 librust-termcolor-1+default-dev (>= 1.1.1-~~)
Provides:
 librust-clap-3+termcolor-dev (= ${binary:Version}),
 librust-clap-3.2+termcolor-dev (= ${binary:Version}),
 librust-clap-3.2.22+termcolor-dev (= ${binary:Version})
Description: Rust Command Line Argument Parser - feature "termcolor"
 This metapackage enables feature "termcolor" for the Rust clap crate, by
 pulling in any additional dependencies needed by that feature.

Package: librust-clap+terminal-size-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-clap-dev (= ${binary:Version}),
 librust-terminal-size-0.1+default-dev (>= 0.1.12-~~)
Provides:
 librust-clap-3+terminal-size-dev (= ${binary:Version}),
 librust-clap-3.2+terminal-size-dev (= ${binary:Version}),
 librust-clap-3.2.22+terminal-size-dev (= ${binary:Version})
Description: Rust Command Line Argument Parser - feature "terminal_size"
 This metapackage enables feature "terminal_size" for the Rust clap crate, by
 pulling in any additional dependencies needed by that feature.

Package: librust-clap+unicase-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-clap-dev (= ${binary:Version}),
 librust-unicase-2+default-dev (>= 2.6-~~)
Provides:
 librust-clap-3+unicase-dev (= ${binary:Version}),
 librust-clap-3.2+unicase-dev (= ${binary:Version}),
 librust-clap-3.2.22+unicase-dev (= ${binary:Version})
Description: Rust Command Line Argument Parser - feature "unicase"
 This metapackage enables feature "unicase" for the Rust clap crate, by pulling
 in any additional dependencies needed by that feature.

Package: librust-clap+unicode-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-clap-dev (= ${binary:Version}),
 librust-clap+unicase-dev (= ${binary:Version}),
 librust-textwrap-0.15+unicode-width-dev
Provides:
 librust-clap-3+unicode-dev (= ${binary:Version}),
 librust-clap-3.2+unicode-dev (= ${binary:Version}),
 librust-clap-3.2.22+unicode-dev (= ${binary:Version})
Description: Rust Command Line Argument Parser - feature "unicode"
 This metapackage enables feature "unicode" for the Rust clap crate, by pulling
 in any additional dependencies needed by that feature.

Package: librust-clap+wrap-help-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-clap-dev (= ${binary:Version}),
 librust-clap+terminal-size-dev (= ${binary:Version}),
 librust-textwrap-0.15+terminal-size-dev
Provides:
 librust-clap-3+wrap-help-dev (= ${binary:Version}),
 librust-clap-3.2+wrap-help-dev (= ${binary:Version}),
 librust-clap-3.2.22+wrap-help-dev (= ${binary:Version})
Description: Rust Command Line Argument Parser - feature "wrap_help"
 This metapackage enables feature "wrap_help" for the Rust clap crate, by
 pulling in any additional dependencies needed by that feature.

Package: librust-clap+yaml-rust-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-clap-dev (= ${binary:Version}),
 librust-yaml-rust-0.4+default-dev (>= 0.4.1-~~)
Provides:
 librust-clap+yaml-dev (= ${binary:Version}),
 librust-clap-3+yaml-rust-dev (= ${binary:Version}),
 librust-clap-3+yaml-dev (= ${binary:Version}),
 librust-clap-3.2+yaml-rust-dev (= ${binary:Version}),
 librust-clap-3.2+yaml-dev (= ${binary:Version}),
 librust-clap-3.2.22+yaml-rust-dev (= ${binary:Version}),
 librust-clap-3.2.22+yaml-dev (= ${binary:Version})
Description: Rust Command Line Argument Parser - feature "yaml-rust" and 1 more
 This metapackage enables feature "yaml-rust" for the Rust clap crate, by
 pulling in any additional dependencies needed by that feature.
 .
 Additionally, this package also provides the "yaml" feature.
